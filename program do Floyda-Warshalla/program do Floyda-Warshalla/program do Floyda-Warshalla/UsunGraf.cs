﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program_do_Floyda_Warshalla
{
    class UsunGraf
    {
        public UsunGraf()
        {
            

            Console.WriteLine("Podaj nazwę pliku do usunięcia: ");
            string plik = Console.ReadLine() + ".txt";
            if (File.Exists(plik))
            {
                File.Delete(plik);
                Console.Clear();
                Console.WriteLine("Plik usunięto: ");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Takiego pliku nie ma.");
            }
        }
    }
}
