﻿using System;
using System.IO;

namespace program_do_Floyda_Warshalla
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Instrukcja do programu: ");
            Console.WriteLine("Graf przechowywany jest w pliku tekstowym w katalogu: " + Directory.GetCurrentDirectory().ToString());
            Console.WriteLine("Pierwszy wiersz zawiera liczbę wierzchołków, oraz liczbę krawędzi w grafie");
            Console.WriteLine("Każdy kolejny wiersz opisuje krawędzie od do waga.");
            Console.WriteLine("Nazwy plików podajemy bez rozszerzenie .txt");

            string wybor;
            ZapisDoPliku zdp;
            UsunGraf ug;
            WypiszGraf wg;
            do
            {
                Console.WriteLine("Menu: \nd - dodaj graf, \nu - usuń graf, \nf - Floyd-Warshall,\np - pokaż dostępne grafy, \nw - wypisz graf,  \nx - koniec programu. ");
                wybor = Console.ReadLine();
                if (wybor == "d")
                {
                    Console.Clear();
                    zdp = new ZapisDoPliku();
                }
                else if (wybor == "u")
                {
                    Console.Clear();
                    ug = new UsunGraf();
                }
                else if (wybor == "f")
                {
                    Console.Clear();
                    Alg_Floyda_Warshalla aFW = new Alg_Floyda_Warshalla();
                    aFW.showAlgFW();
                }
                else if (wybor == "p")
                {
                    int licz = 1;
                    Console.Clear();
                    Console.WriteLine("Masz do dyspozycji następujace grafy: ");
                    string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory());
                    foreach (var item in filePaths)
                    {
                        string plik2 = Path.GetFileName(item);
                        if (plik2.Split('.')[plik2.Split('.').Length - 1] == "txt")
                            Console.WriteLine("Graf nr " + licz++ + ": " + Path.GetFileName(item.Substring(0,item.Length-4)));
                    }
                }
                else if (wybor=="w")
                {
                    Console.Clear();
                    wg = new WypiszGraf();
                }
                else if (wybor == "x")
                {
                    Console.Clear();
                    return;
                }
            } while (wybor != "x");
            Console.ReadKey();
        }
    }
}
