﻿using System;
using System.Collections.Generic;
using System.IO;

namespace program_do_Floyda_Warshalla
{
    class Alg_Floyda_Warshalla
    {

        List<List<int>> d;
        List<List<int>> p;
        int n;
        int m;
        string wejscie;

        public Alg_Floyda_Warshalla()
        {
            Console.WriteLine("Podaj nazwę pliku wejściowego: ");
            wejscie = Console.ReadLine();
            if (!File.Exists(wejscie + ".txt"))
            {
                Console.Clear();
                Console.WriteLine("Plik " + wejscie + ".txt" + " nie istnieje.");
            }
            else
            {
                Console.Clear();
                string[] tablicaStringow;
                StreamReader sR = new StreamReader(wejscie + ".txt");
                string linia = sR.ReadLine();
                tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                n = int.Parse(tablicaStringow[0]);
                m = int.Parse(tablicaStringow[1]);
                List<List<List<int>>> listaDP = PobieraczGrafu.zczytajZPliku(wejscie);
                d = listaDP[0];
                p = listaDP[1];
            }
        }
        public bool floudWarshall()
        {
            for (int k = 0; k < n; k++)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if ((d[i][k] == Int16.MaxValue) || (d[k][j] == Int16.MaxValue)) continue;
                        if (d[i][j] > d[i][k] + d[k][j])
                        {
                            Console.WriteLine("\nRelaksacja przez wierzchołek k: " + k + " między wierzchołkami " + i + " -> " + j);
                            Console.WriteLine("d[" + i + "][" + j + "] > d[" + i + "][" + k + "] + d[" + k + "][" + j + "] <==> "
                                + d[i][j] + " > " + d[i][k] + " + " + d[k][j]);
                            Console.WriteLine("d[" + i + "][" + j + "] = d[" + i + "][" + k + "] + d[" + k + "][" + j + "] <==> "
                                + "d[" + i + "][" + j + "] = " + (d[i][k] + d[k][j]));
                            d[i][j] = d[i][k] + d[k][j];
                            p[i][j] = p[k][j];
                            PobieraczGrafu.wypiszMacierze(d, p, n);
                        }
                    }
                    
                }
            }
            for (int i = 0; i < n; i++)
                if (d[i][i] < 0) return false; // Ujemny cykl
            return true;
        }


        public void FWPath(int i, int j)
        {
            if (i == j) Console.Write(i + " ");
            else if (p[i][j] == -1) Console.Write("Brak ścieżki");
            else
            {
                FWPath(i, p[i][j]);
                Console.Write(j + " ");
            }
        }


        public void showAlgFW()
        {
            if (floudWarshall())
            {
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        if (i != j)

                        {
                            Console.Write(i + "-" + j + " : ");
                            FWPath(i, j);
                            if (d[i][j] < Int16.MaxValue) Console.Write("Koszt wynosi: " + d[i][j]);
                            Console.WriteLine();
                        }
            }
            else Console.Write("Znalezniono cyklujemny\n");

            //   Console.WriteLine();

        }

    }
}

