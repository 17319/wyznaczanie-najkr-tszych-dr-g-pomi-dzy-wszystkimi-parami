﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program_do_Floyda_Warshalla
{
    class ZapisDoPliku
    {
        string nazwaPliku = "";
        public ZapisDoPliku()
        {
            try
            {

                Int32 v = 0, e = 0, a, b, w;
                string wybor;
                Console.WriteLine("\nPodaj nazwę pliku, pusta nazwa oznacza rezygnację z wprowadzania danych");
                nazwaPliku = Console.ReadLine();
                if (nazwaPliku.Length == 0)
                    throw new Exception("Pusta nazwa pliku");
                if (File.Exists(nazwaPliku + ".txt"))
                {
                a:
                    Console.WriteLine("Plik o nazwie " + nazwaPliku + ".txt już istnieje, czy chcesz nadpisać ten plik y/n");
                    wybor = Console.ReadLine();
                    if (wybor == "n" || wybor == "N")
                        return;
                    if (wybor == "y" || wybor == "Y")
                        Console.WriteLine("Plik zostanie nadpisany");
                    else
                    {
                        Console.WriteLine("Błędny wybór: ");
                        goto a;
                    }

                }
                System.IO.StreamWriter file = new System.IO.StreamWriter(nazwaPliku + ".txt");
                Console.WriteLine("Podaj liczbę wierzchołków w grafie: ");
                if (Int32.TryParse(Console.ReadLine(), out v))
                {
                    Console.WriteLine("Podaj liczbę krawędzi w grafie: ");
                    if (Int32.TryParse(Console.ReadLine(), out e))
                    {
                        file.WriteLine(v + " " + e);
                        int licz = 0;
                        while (true)
                        {
                            Console.WriteLine("Podaj krwędź nr " + (licz + 1) + " w postaci: vA vB waga");
                            String abw = Console.ReadLine();
                            if (Int32.TryParse(abw.Split(' ')[0], out a) &&
                             Int32.TryParse(abw.Split(' ')[1], out b) &&
                             Int32.TryParse(abw.Split(' ')[2], out w))
                            {
                                file.WriteLine(a + " " + b + " " + w);
                                e--;
                                licz++;
                                if (e == 0)
                                    break;
                            }
                            else
                            {
                                file.Close();
                                if (File.Exists(nazwaPliku + ".txt"))
                                {

                                    File.Delete(nazwaPliku + ".txt");
                                }
                                throw new Exception("Dane należy wpisywać w postaci: v1 v2 w, nie zachowano tej zasady.");
                            }
                        }
                    }
                    else
                    {
                        file.Close();
                        if (File.Exists(nazwaPliku + ".txt"))
                        {
                            File.Delete(nazwaPliku + ".txt");
                        }
                        throw new Exception("Błędnie wprowadzono liczbę krwawędzi");
                    }
                }
                else
                {
                    file.Close();
                    if (File.Exists(nazwaPliku + ".txt"))
                    {

                        File.Delete(nazwaPliku + ".txt");
                    }
                    throw new Exception("Błędnie wprowadzono liczbę wierzchołków");
                }
                file.Close();
                Console.Clear();
                Console.WriteLine("Graf zapisany do pliku");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + ", graf nie został dodany, jeśli chesz rozpocznij wprowadzanie grafu od nowa.");
            }
        }
    }
}
