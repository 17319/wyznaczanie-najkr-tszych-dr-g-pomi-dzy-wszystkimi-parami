﻿using System;
using System.Collections.Generic;
using System.IO;

namespace program_do_Floyda_Warshalla
{
    class PobieraczGrafu
    {

        public static List<List<List<int>>> zczytajZPliku(string nazwaPliku)
        {
            try
            {
                List<List<int>> d = new List<List<int>>();
                List<List<int>> p = new List<List<int>>();
                string[] tablicaStringow;
                StreamReader sR = new StreamReader(nazwaPliku + ".txt");
                string linia = sR.ReadLine();
                tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                int v = int.Parse(tablicaStringow[0]);
                int e = int.Parse(tablicaStringow[1]);

                for (int i = 0; i < v; i++)
                {
                    d.Add(new List<int>());
                    p.Add(new List<int>());
                    for (int j = 0; j < v; j++)
                    {
                        d[i].Add(Int16.MaxValue);
                        p[i].Add(-1);
                    }
                    d[i][i] = 0;
                }
                wypiszMacierze(d, p, v);

                for (int i = 0; i < e; i++)
                {
                    linia = sR.ReadLine();
                    tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    int x = int.Parse(tablicaStringow[0]);
                    int y = int.Parse(tablicaStringow[1]);
                    int w = int.Parse(tablicaStringow[2]);
                    d[x][y] = w;
                    Console.WriteLine("Dodajemy: " + x + " -> " + y + " (" + w + ")  Krawędź " + (i + 1) + " z " + e);
                    p[x][y] = x;
                    wypiszMacierze(d, p, v);
                }
                List<List<List<int>>> listaDP = new List<List<List<int>>>();
                listaDP.Add(d);
                listaDP.Add(p);
                return listaDP;
            }
            catch (Exception)
            {
                Console.WriteLine("Nie zczytano danych z pliku");
                return new List<List<List<int>>>();
            }
        }

        public static void wypiszMacierze(List<List<int>> d, List<List<int>> p, int m)
        {
            Console.WriteLine("Macierz kosztów dojścia: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("d" + "\t");
            for (int i = 0; i < m; i++)
                Console.Write(i + "\t");
            for (int i = 0; i < m; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                for (int j = 0; j < m; j++)
                {
                    if (j == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("\t" + d[i][j] + "\t");
                    }
                    else
                        Console.Write(d[i][j] + "\t");
                }
            }
            Console.WriteLine("\nMacierz poprzedników: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("p" + "\t");
            for (int i = 0; i < m; i++)
                Console.Write(i + "\t");
            for (int i = 0; i < m; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                for (int j = 0; j < m; j++)
                {
                    if (j == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("\t" + p[i][j] + "\t");
                    }
                    else
                        Console.Write(p[i][j] + "\t");
                }
            }
            Console.WriteLine();
            Console.WriteLine("==========================================================================");
            Console.WriteLine("==========================================================================");

            czekajNaSpacje();
        }

        private static void czekajNaSpacje()
        {
            Console.WriteLine("Spacja - dalej, Enter - dalej i czyścimy konsole");
            ConsoleKeyInfo cki;
            do
            {
                cki = Console.ReadKey();
                if (cki.Key == ConsoleKey.Spacebar)
                    break;
                else if (cki.Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    break;
                }
            } while (true);
        }
    }
}
